//
//  ContributorsApi.swift
//  RESTConsumerApp
//
//  Created by Cristian Gutu on 6/8/17.
//  Copyright © 2017 Cristian Gutu. All rights reserved.
//

import Foundation
import PromiseKit
import AwaitKit

public class ContributorsApi {
    
    private let commonApi = CommonApi()
    
    init() {
        
    }
    
    public func fetchRepoContributors(repoOwnerName: String,
                                      repoName: String,
                                      pageNumber: Int) -> Promise<Array<Contributor>> {
        return async {
            let url = "https://api.github.com/repos/" + repoOwnerName + "/" + repoName + "/contributors" + "?page=" + String(pageNumber)
            let contributorsJson = try await(self.commonApi.makeGETRequest(url: url))
            
            var contributorsObjects: Array<Contributor> = []
            for jsonObject in contributorsJson {
                let contributor: Contributor = Contributor.createInstanceFromJson(jsonValue: jsonObject.1)
                contributorsObjects.append(contributor)
            }
            return contributorsObjects
        }
    }
    
}
