//
//  BasicError.swift
//  RESTConsumerApp
//
//  Created by Cristian Gutu on 6/8/17.
//  Copyright © 2017 Cristian Gutu. All rights reserved.
//

import Foundation
public protocol BasicError : Error {
    
    var message: String { get }
    var code: Int { get }
}
