//
//  Contributor.swift
//  RESTConsumerApp
//
//  Created by Cristian Gutu on 6/8/17.
//  Copyright © 2017 Cristian Gutu. All rights reserved.
//

import Foundation
import SwiftyJSON

public class Contributor {
    
    public var id: String
    public var username: String
    public var numberOfContributions: Int
    
    init(id: String, username: String, numberOfContributions: Int){
        self.id = id
        self.username = username
        self.numberOfContributions = numberOfContributions
    }
    
    
    public static func createInstanceFromJson(jsonValue: JSON) -> Contributor {
        let contributor = Contributor(
            id: jsonValue["id"].stringValue,
            username: jsonValue["login"].stringValue,
            numberOfContributions: jsonValue["contributions"].intValue
        )
        return contributor
    }
    
    
}
