//
//  Repository.swift
//  RESTConsumerApp
//
//  Created by Cristian Gutu on 6/8/17.
//  Copyright © 2017 Cristian Gutu. All rights reserved.
//

import Foundation
import SwiftyJSON

public class Repository {
    
    public var id: String
    public var name: String
    public var fullName: String
    
    init(id: String, name: String, fullName: String) {
        self.id = id
        self.name = name
        self.fullName = fullName
    }
    
    public static func createInstanceFromJson(jsonValue: JSON) -> Repository {
        let repository = Repository(
            id: jsonValue["id"].stringValue,
            name: jsonValue["name"].stringValue,
            fullName: jsonValue["full_name"].stringValue
        )
        return repository
    }
}
