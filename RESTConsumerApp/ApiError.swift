//
//  ApiError.swift
//  RESTConsumerApp
//
//  Created by Cristian Gutu on 6/8/17.
//  Copyright © 2017 Cristian Gutu. All rights reserved.
//

import Foundation
public class ApiError: BasicError {
    
    public var message: String
    public var code: Int
    
    init(code: Int){
        self.message = "An error has occurred"
        self.code = code
    }

}
