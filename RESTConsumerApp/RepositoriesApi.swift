//
//  PublicRepositoriesApi.swift
//  RESTConsumerApp
//
//  Created by Cristian Gutu on 6/8/17.
//  Copyright © 2017 Cristian Gutu. All rights reserved.
//

import Foundation
import PromiseKit
import AwaitKit

public class RepositoriesApi {
    
    private let commonApi = CommonApi()
    
    init() {
        
    }
    
    public func fetchPublicRepositories() -> Promise<Array<Repository>> {
        return async {
            let repositoriesJson = try await(self.commonApi.makeGETRequest(url: "https://api.github.com/repositories"))
            var repositoriesObjects: Array<Repository> = []
            for jsonObject in repositoriesJson {
                let repository: Repository = Repository.createInstanceFromJson(jsonValue: jsonObject.1)
                repositoriesObjects.append(repository)
            }
            return repositoriesObjects
        }
    }
    
    public func createRepository() -> Promise<Repository> {
        return async {
            let newRepoParams: [String:Any] = [
                "name": "Hey yo22",
                "description": "This a repooouw",
                "homepage": "https://3pillarglobal.com"
            ]
            let createRepoJsonReponse = try await(self.commonApi.makePOSTRequest(url: "https://api.github.com/user/repos?access_token=305caf451ef20bd10c477117cee96c355df20dbb", params: newRepoParams))
            return Repository.createInstanceFromJson(jsonValue: createRepoJsonReponse)
        }
    }
}
