//
//  ViewController.swift
//  RESTConsumerApp
//
//  Created by Cristian Gutu on 6/8/17.
//  Copyright © 2017 Cristian Gutu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
// Fetch a list of public repos
        
//        RepositoriesApi().fetchPublicRepositories().then { (repositories) -> Void in
//            let a = repositories;
//        }
    
        
// Fetch a list of contributors for a specific repo
        
//        ContributorsApi().fetchRepoContributors(repoOwnerName: "Alamofire", repoName: "Alamofire", pageNumber: 0).then {
//            (contributorsList) -> Void in
//            let a = contributorsList
//            print(a)
//        }.catch { (error) in
//            let b = error
//            print(b)
//        }
        
// Creates a new repo using a my GitHub Account access token
        
//        RepositoriesApi().createRepository().then { (newRepo) -> Void in
//            let a = newRepo
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

