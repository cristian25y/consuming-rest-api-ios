//
//  SuperApi.swift
//  RESTConsumerApp
//
//  Created by Cristian Gutu on 6/8/17.
//  Copyright © 2017 Cristian Gutu. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SwiftyJSON

public class CommonApi {
    
    func makeGETRequest(url: String) -> Promise<JSON> {
        return Promise {
            fulfill, reject in
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { response in
                if response.result.isSuccess {
                    fulfill(JSON(response.result.value ?? ""))
                } else {
                    reject(ApiError(code: 32))
                }
            }
        }
        
    }
    
    func makePOSTRequest(url: String, params: [String:Any]) -> Promise<JSON>{
        return Promise {
            fulfill, reject in
            Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { response in
                if response.result.isSuccess {
                    fulfill(JSON(response.result.value ?? ""))
                } else {
                    reject(ApiError(code: 32))
                }
            }
        }
    }
}
